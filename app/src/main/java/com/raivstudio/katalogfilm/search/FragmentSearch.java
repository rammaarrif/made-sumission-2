package com.raivstudio.katalogfilm.search;

import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.content.Loader;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.raivstudio.katalogfilm.ItemFilm;
import com.raivstudio.katalogfilm.R;

import java.util.ArrayList;

public class FragmentSearch extends Fragment implements
        LoaderManager.LoaderCallbacks<ArrayList<ItemFilm>> {

    EditText inputjdl;
    ImageView gbposter,btncari;
    ListView listView;
    AdapterFilm adapter ;

    static final String EXTRAS_FILM = "EXTRAS_FILM";

        public FragmentSearch(){

        }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        adapter = new AdapterFilm(getActivity());
        adapter.notifyDataSetChanged();
        listView = view.findViewById(R.id.lvFilm);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    final int position, long id) {

                ItemFilm item = (ItemFilm) parent.getItemAtPosition(position);

                Intent pindah = new Intent(getActivity(), DetailFilm.class);
                pindah.putExtra(DetailFilm.Extra_Judul, item.getJdlfilm());
                pindah.putExtra(DetailFilm.Extra_tanggal, item.getTglfilm());
                pindah.putExtra(DetailFilm.Extra_rating, item.getRatefilm());
                pindah.putExtra(DetailFilm.Extra_Sinopsis, item.getSinopsis());
                pindah.putExtra(DetailFilm.Extra_poster, item.getGbrfilm());
                pindah.putExtra(DetailFilm.Extra_backdrop, item.getGbrbackdrop());
                startActivity(pindah);
            }
        });

        inputjdl = view.findViewById(R.id.edt_judul);
        btncari = view.findViewById(R.id.tmblcari);
        gbposter = view.findViewById(R.id.gbposter);
        btncari.setOnClickListener(movieListener);

        String judul_film = inputjdl.getText().toString();

        Bundle bundle = new Bundle();
        bundle.putString(EXTRAS_FILM, judul_film);

        getLoaderManager().initLoader(0, bundle, this);
        return view;
    }


    @Override
    public Loader<ArrayList<ItemFilm>> onCreateLoader(int i, Bundle bundle) {
        String judulFilm = "";
        if (bundle != null){
            judulFilm = bundle.getString(EXTRAS_FILM);
        }
        return new AsynctaskLoaderFilm(getActivity(), judulFilm);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<ItemFilm>> loader, ArrayList<ItemFilm> itemFilm) {
        adapter.setData(itemFilm);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<ItemFilm>> loader) {
        adapter.setData(null);
    }

    View.OnClickListener movieListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String _judulMovie = inputjdl.getText().toString();
            if(TextUtils.isEmpty(_judulMovie)){
                Toast.makeText(getActivity(), "Isikan judul film lebih dulu !",
                        Toast.LENGTH_SHORT).show();
            }

            Bundle bundle = new Bundle();
            bundle.putString(EXTRAS_FILM, _judulMovie);

            getLoaderManager().restartLoader(0, bundle, FragmentSearch.this);

        }
    };
}
